<?php

/**
 * @file
 * This module integrates the jQuery DataTables to Drupal
 * without requirement of Views.
 * 
 * Fow now only one type of table is generated: FixedColumns.
 * @link 
 * http://datatables.net/release-datatables/extras/FixedColumns/css_size.html 
 * @endlink
 * 
 * @todo Parametrize the table creation process in order to make possible
 * to build tables with different configurations.
 * 
 * Author: Ranieri Machado
 * 
 *  
 */


/**
 * Prefix to be added to table name on blocks' naming.
 */
const AK_SCROLLING_BLOCK_PREFIX = 'akst_block__';

/**
 * Used as flag that informs that the table uses a customized view mode.
 */
const AK_SCROLLING_CUSTOM_TEXT =  'custom';

/**
 * Implements hook_permission().
 */
function ak_scrolling_tables_permission() {
  return array(
    'administer ak_scrolling_tables' => array(
      'title' => t('Administer lm_list'),
      'description' => t('Perform administer tasks on ak_scrolling_tables.'),
    ),
    'view ak_scrolling_tables' => array(
      'title' => t('View lm_list'),
      'description' => t('View ak_scrolling_tables'),
    ),    
  );  
}


/**
 * Implements hook_menu().
 */
function ak_scrolling_tables_menu() {
  $items['admin/structure/ak_scrolling_tables'] = array(
    'title' => t('AK Scrolling Tables'),
    'page callback' => 'ak_scrolling_tables_list_all',
    'access arguments' => array('administer ak_scrolling_tables'),  
    'type' => MENU_NORMAL_ITEM,
  );  
  $items['admin/structure/ak_scrolling_tables/add'] = array(
    'title' => t('Add AK Scrolling Table'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ak_scrolling_tables_add_form'),
    'access arguments' => array('administer ak_scrolling_tables'),  
    'type' => MENU_LOCAL_ACTION,
  );  
  $items['admin/structure/ak_scrolling_tables/arrange_fields'] = array(
    'title' => t('Arranging Fields of the Scrolling Table'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ak_scrolling_tables_arrange_fields_form'),
    'access arguments' => array('administer ak_scrolling_tables'),  
    'type' => MENU_CALLBACK,
  );   
  $items['admin/ak_scrolling_tables/edit'] = array(
    'title' => t('Edit AK Scrolling Table'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ak_scrolling_tables_edit_form'),
    'access arguments' => array('administer ak_scrolling_tables'),   
    'type' => MENU_CALLBACK,  
  );  
  $items['admin/ak_scrolling_tables/delete'] = array(
    'title' => t('Delete AK Scrolling Table'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ak_scrolling_tables_delete_form'),
    'access arguments' => array('administer ak_scrolling_tables'),   
    'type' => MENU_CALLBACK,  
  );  
  
  $tables = ak_scrolling_tables_get_all_tables();
  
  if ($tables) {    
    foreach ($tables as $table) {
      $items[$table->path] = array(
        'title' => $table->name,
        'page callback' => 'ak_scrolling_tables_display',
        'page arguments' => array($table->machine_name),
        'access arguments' => array('view ak_scrolling_tables'),   
        'type' => MENU_CALLBACK,  
      );        
    }
  }
  
  return $items;
}

/**
 * Implements hook_theme().
 */
function ak_scrolling_tables_theme() {
  return array(
    // Theme function for the 'arrange fields' form
    'ak_scrolling_tables_arrange_fields_form' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Display the table
 * 
 * @param type $arg: machine name of the table
 * 
 */
function ak_scrolling_tables_display($machine_name, $block = FALSE ) {
  
  $table_id = 'akst-' . str_replace('_', '-', $machine_name);
  $table_id .= ($block) ? '-block' : '';
  
  ak_scrolling_tables_add_js($table_id, $machine_name) ;
  
  $table_info = ak_scrolling_tables_get_table_info($machine_name);  

  $table = new AKDataTable;
  $table->content_type = $table_info['content_type'];
  $table->fields = unserialize($table_info['fields_order']);
  $table->class_prefix = 'akst-col';
  $table->id = $table_id;
  
  if ($table_info['view_mode']==AK_SCROLLING_CUSTOM_TEXT) {
    $table->view_mode = ak_scrolling_tables_get_machine_name($table_info['custom_view_mode']);
  } else {
    $table->view_mode = $table_info['view_mode'];
  }  
  
  return $table->table();  

}

/**
 * Adds all JavaScript files
 */
function ak_scrolling_tables_add_js($table_id, $machine_name) {
  
  $module_dir = drupal_get_path('module', 'ak_scrolling_tables');
  drupal_add_js($module_dir . '/js/FixedColumns.min.js');
  drupal_add_js($module_dir . '/js/jquery.dataTables.min.js');
  $js = file_get_contents($module_dir . '/js/initTable.js' );
  $js = str_replace('AKSTTableVAR', 'oTable_' . $machine_name , $js);
  $js = str_replace('AKSTTableID', '"#' . $table_id. '"', $js);
  drupal_add_js($js,'inline');

} 

/**
 * Db call for listing all
 * 
 * @return array of objects 
 */
function ak_scrolling_tables_get_all_tables() {
  
    return db_select('ak_scrolling_tables', 'a')
      ->fields('a')
      ->execute()
      ->fetchAll(); 
    
}

/**
 * List all defined scrolling tables
 * 
 * @return $output: Content 
 * 
 */
function ak_scrolling_tables_list_all() {
  
  $rows = ak_scrolling_tables_get_all_tables(); 
  
  $entity_info = entity_get_info('node');
  $view_modes = $entity_info['view modes'];  

  
  $table_rows = array();
  
  foreach ($rows as $row) {
    $name = '<b>' . $row->name . '</b><br />(' . $row->machine_name . ')';
    $type = node_type_get_name($row->content_type); 
//    $type = node_type_get_name($row->content_type) . '<br />(' . $row->content_type . ')'; 
    if ($row->view_mode==AK_SCROLLING_CUSTOM_TEXT) {
      $view_mode = $row->custom_view_mode;
    }
    else {
      $view_mode = $view_modes[$row->view_mode]['label'];
    }
    $path = l($row->path, $row->path);
    $op = '<ul style="list-style-type:none; margin:0;">';
    $op .= '<li>' . l(t('Edit'), 'admin/ak_scrolling_tables/edit/' . $row->machine_name ) . '</li>'; 
    $op .= '<li>' . l(t('Delete'), 'admin/ak_scrolling_tables/delete/' . $row->machine_name )  . '</li>'; 
    $op .= '<li>' . l(t('Arrange fields'), 'admin/structure/ak_scrolling_tables/arrange_fields/' . $row->machine_name ) . '</li>';
    $op .= '</ul>' ;
   
    $block = ($row->block) ? t('Yes') : t('No');
    $table_rows[] = array($name, $row->description, $type, $view_mode, $path, $block, $op);
  }    

  $header = array('Table Name', 'Description', 'Content Type', 'View mode', 'Path', 'Block', 'Operations');
  
  $output = theme('table', array(
    'header' => $header, 
    'rows' => $table_rows, 
    'attributes' => array('class' => array('akst-list')),
  )); 
  
 return $output;
 
}

/**
 * 
 * Implements hook_form().
 * 
 * Form for adding tables
 * 
 * @see ak_scrolling_tables_get_content_fields()
 * 
 * Variables:
 * 
 * $fields_list: Gotten from ak_scrolling_tables_get_content_fields. Array with
 * two elements:
 *    $fields_list['labels']: List of descriptions of content types and its
 *    machine names. 
 *    $fields_list['names']: List of machine names of content types.
 * 
 * $form['akst_fields']['#akst_machine_names']: Non default form property.  
 * Created in this specific context in order to tranport all fields' machine
 * names to ak_scrolling_tables_add_form_submit().
 * 
 */
function ak_scrolling_tables_add_form($form, &$form_state) {
  
  $entity_info = entity_get_info('node');
  $view_modes = $entity_info['view modes'];
  $view_modes_list = array(AK_SCROLLING_CUSTOM_TEXT => t('Custom'));
  foreach ($view_modes as $key => $view_mode) {
     $view_modes_list[$key]= $view_mode['label'];
  }  
  
  $form['akst_name'] = array(
    '#title' => t('Table name'),
    '#type' => 'textfield',
    '#size' => 64,
    '#maxlength' => 64,      
    '#required' => TRUE,
  );    
  
  $form['akst_machine_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Machine name'),
    '#default_value' => '',
    '#maxlength' => 64,
    '#description' => t('A unique name to construct the URL for the Scrolling Table page and/or Block'),
    '#machine_name' => array(
      'exists' => 'ak_scrolling_tables_machine_name_exists',
      'source' => array('akst_name'),
    ),
  );  
  $form['akst_description'] = array(
    '#title' => t('Description'),
    '#description' => t('A brief description of the Scrolling Table. (optional)'),  
    '#type' => 'textfield',
    '#size' => 120,
    '#maxlength' => 255,      
    '#required' => FALSE,
  );   
  $form['akst_type'] = array(
    '#title' => t('Content type'),        
    '#type' => 'select',
    //Get all content types  
    '#options' => node_type_get_names(),
    '#description' => t(''),   
    '#ajax' => array(
      'callback' => 'ak_scrolling_tables_add_types_ajax_callback',
      'wrapper' => 'akst-content-type-fields',
    ),       
    '#required' => TRUE,
  );  
  $form['akst_fields'] = array(
    '#title' => '',
    '#type' => 'checkboxes',
    '#options' => array(),
    '#prefix' => '<div id="akst-content-type-fields">',
    '#suffix' => '</div>',
    '#akst_machine_names' => array(),
    '#required' => FALSE,      
  );   
  $form['akst_path'] = array(
    '#title' => t('Custom path'),
    '#description' => t('A unique path to the page displaying the Scrolling Table.<br />
      Optional. If not supplied the machine name will be used.'),  
    '#type' => 'textfield',
    '#size' => 64,
    '#maxlength' => 64,   
    '#required' => FALSE,
  );    
  $form['akst_view_mode'] = array(
    '#title' => t('View mode'),        
    '#type' => 'select',
    '#options' => $view_modes_list,
    '#default_value' => 'teaser',
    '#description' => t('The view mode to be used when displaying the Scrolling Table.<br />
      If "Custom" is chosen the name of a custom view mode should be supplied. '),   
    '#ajax' => array(
      'callback' => 'ak_scrolling_tables_add_custom_view_mode_ajax_callback',
      'wrapper' => 'akst-custom-view-mode',
    ),       
    '#required' => FALSE,
  );    
  $form['akst_custom_view_mode'] = array(
    '#title' => t('Custom view mode'),
    '#description' => t('A unique name for the custom view mode.'),  
    '#type' => 'hidden',
    '#size' => 32,
    '#maxlength' => 32,   
    '#prefix' => '<div id="akst-custom-view-mode">',
    '#suffix' => '</div>',      
    '#required' => FALSE,
  );      
  $form['akst_block'] = array(
      '#title' => 'Create a Block',
      '#description' => t('Whether or not create a specific block for this Scrolling Table.'),
      '#type' => 'checkbox',
      '#default-value' => FALSE,
  );  
  
  
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['add'] = array('#type' => 'submit', '#value' => t('Add Scrolling Table'), '#name' => 'buttom_add');
  $form['actions']['arrange'] = array('#type' => 'submit', '#value' => t('Add Scrolling Table and arrange fields'), '#name' => 'button_arrange');
  
  if (isset($form_state['values']['akst_type'])) { 
    $fields_list = ak_scrolling_tables_get_content_fields($form_state['values']['akst_type']);
    $form['akst_fields']['#options'] = $fields_list['labels'];
    $form['akst_fields']['#akst_machine_names'] = $fields_list['names'];
    $form['akst_fields']['#title'] = t('Fields');
    $form['akst_fields']['#required'] = TRUE;      
  }
 
  if ( isset($form_state['values']['akst_view_mode']) && $form_state['values']['akst_view_mode']==AK_SCROLLING_CUSTOM_TEXT) {
    $form['akst_custom_view_mode']['#type'] = 'textfield';
    $form['akst_custom_view_mode']['#required'] = TRUE;          
  }
  
  return $form;
}

/**
 * 
 * @param $form
 * @param $form_state
 * @param $arg
 * @return type
 * 
 */
function ak_scrolling_tables_arrange_fields_form($form, &$form_state, $arg) {
  
  $form['fields_items']['#tree'] = TRUE;

  $table_info = ak_scrolling_tables_get_table_info($arg);
  $fields_list = ak_scrolling_tables_get_content_fields($table_info['content_type']);      
  $fields = unserialize($table_info['fields_order']);
  
  $delta = floor((count($fields) - 0.5) / 2 ) * 10;
  $weight = $delta * -1;
  
  foreach ($fields as $field) {
    
    $key = array_search($field, $fields_list['names']);
    
    $form['fields_items'][$field] = array(
 
      'name' => array(
        '#markup' => $fields_list['labels'][$key],
      ),

      'description' => array(
        '#markup' => $field,
      ),        
        
      'weight' => array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => $weight,
        '#delta' => $delta + 10,
        '#title_display' => 'invisible',
      ),
    ); 
    $weight += 10;
  }

  $form['akst_machine_name'] = array (
    '#type' => 'hidden',
    '#value' => $table_info['machine_name'],
  );      

  $form['akst_name'] = array (
    '#type' => 'item',
    '#title' => t('Table name'),
    '#markup' => $table_info['name'],
  );   

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save Changes'));  
  $form['actions']['cancel'] = array('#markup' =>  l(t('Cancel'),'admin/structure/ak_scrolling_tables'));  
  
  return $form;
  
}

function theme_ak_scrolling_tables_arrange_fields_form($variables) {

  $form = $variables['form'];
  $rows = array();

  foreach (element_children($form['fields_items']) as $id) {
    $form['fields_items'][$id]['weight']['#attributes']['class'] = array('akst-item-weight');
    $rows[] = array(
      'data' => array(
        drupal_render($form['fields_items'][$id]['name']),
        drupal_render($form['fields_items'][$id]['description']),
        drupal_render($form['fields_items'][$id]['weight']),
      ),
      'class' => array('draggable'),
    );
  }

  $header = array(t('Name'), t('Machine-name'), t('Weight'));
  $table_id = 'akst-items-table';

  $output = drupal_render($form['akst_name'] );
  
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));
  
  $output .= drupal_render_children($form);
 
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'akst-item-weight');

  return $output;
}

/**
 * 
 */
function ak_scrolling_tables_arrange_fields_form_submit($form, &$form_state) {
  // Because the form elements were keyed with the item ids from the database,
  // we can simply iterate through the submitted values.
  $fields_order = array();
  foreach ($form_state['values']['fields_items'] as $id => $item) {
    $fields_order[$item['weight']] = $id;
  }
  ksort($fields_order);
  if (ak_scrolling_tables_arrange_fields_update($form_state['values']['akst_machine_name'], $fields_order)) {
    drupal_set_message(t('Fields arranged.'));
    drupal_goto('admin/structure/ak_scrolling_tables');
  }
  else {
    form_set_error('', t('Error while updatinf fields arrangement.'));        
  }
 
}

function ak_scrolling_tables_arrange_fields_update($machine_name, $fields_order) {
  try {
    $result = db_update('ak_scrolling_tables')
      ->fields(
        array(
          'fields_order' => serialize($fields_order),              
        )
      )
      ->condition('machine_name', $machine_name)      
      ->execute();
    return TRUE;  
  } catch (Exception $e) {
    return FALSE;  
  }     
}


/**
 * AJAX Callback
 * 
 * @param type $form
 * @param type $form_state
 * @return type $form['akst_custom_view_mode']
 * 
 */
function ak_scrolling_tables_add_custom_view_mode_ajax_callback(&$form, $form_state) {
  return $form['akst_custom_view_mode'];
}

/**
 * AJAX Callback
 * 
 * @param type $form
 * @param type $form_state
 * @return type $form['akst_fields']
 * 
 */
function ak_scrolling_tables_add_types_ajax_callback(&$form, $form_state) {
  return $form['akst_fields'];
}

/**
 * Check if machine name for the table is already being used * 
 * 
 * @param  $machine_name
 * @return type boolean
 * 
 */
function ak_scrolling_tables_machine_name_exists($machine_name) {
  $query = db_select('ak_scrolling_tables', 'a')
    ->fields('a', array('machine_name'))
    ->condition('a.machine_name', $machine_name);    
  return $result = $query->execute()->fetchAssoc();
}

/**
 * 
 * @param $type
 * @return $fields_list: Array with two elements:
 *    $fields_list['labels']: List of descriptions of content types and its
 *    machine names. 
 *    $fields_list['names']: List of machine names of content types.
 * 
 */
function ak_scrolling_tables_get_content_fields($type) {
  //Get all fields of the content type     
  $fields = field_info_instances('node', $type );
  $types = node_type_get_types();
  $fields_list = array();
  if (isset($types[$type]) && $types[$type]->has_title) {
    $fields_list['labels'][] = $types[$type]->title_label;
    $fields_list['names'][] = 'title';
  }  
  foreach ($fields as $key => $field) {
    //$fields_list['labels'][] = $field['label'] . ' (' . $key . ')';
    $fields_list['labels'][] = $field['label'];
    $fields_list['names'][] = $key;
  }
  return $fields_list;
}

/**
 * Implements hook_form_validade
 */
function ak_scrolling_tables_add_form_validate($form, &$form_state) {
  //Check if the path is already in use
  if ($form_state['input']['akst_view_mode']==AK_SCROLLING_CUSTOM_TEXT && trim($form_state['input']['akst_custom_view_mode'])=='') {
    form_set_error('akst_custom_view_mode', t('Custom view mode field is required.'));      }
  if (trim($form_state['input']['akst_path'])!='' && drupal_valid_path($form_state['input']['akst_path'], FALSE)) {
    form_set_error('akst_path', t('Invalid Custom Path.'));    
  }    
  $form_state['values'] = ak_scrolling_tables_sanitize_form($form, $form_state['values']);
}

/**
 * Search for texts fields ans sanitizes its content
 * 
 * @param type $form
 * @param type $values
 * @return sanitized array $values
 * 
 */
function ak_scrolling_tables_sanitize_form($form, $values) {
  foreach ($form as $key => $element) {
    if (preg_match('/akst_/',$key) &&  in_array($element['#type'], array('textfield', 'text'))) {
      $values[$key] = check_plain(filter_xss($values[$key] ));  
    }    
  }  
  return $values;
}


/**
 * Implements hook_form_submit
 * 
 * Variables:
 * 
 * $form_state['values']['akst_chosen_ones']: A non standart form element  
 * to be sent to ak_scrolling_tables_add().
 * 
 */
function ak_scrolling_tables_add_form_submit($form, &$form_state) {
  $fields = $form_state['values']['akst_fields'];
  $chosen_ones = array();
  //Count how many fields were chosen
  foreach ($fields as $key => $checked) {
    if ($checked!='') {
      $chosen_ones[] = $form['akst_fields']['#akst_machine_names'][$key];
    }
  }  
  //Creates a non standart element in $form_state['values'] in order to be
  //sent to ak_scrolling_tables_add.
  $form_state['values']['akst_chosen_ones'] = $chosen_ones;  
  if (ak_scrolling_tables_add($form_state['values']))  {
    drupal_set_message(t('The scrolling table ' . $form_state['values']['akst_name'] . ' has been created.' ));
    if (isset($form_state['values']['button_arrange'])) {      
      drupal_goto('admin/structure/ak_scrolling_tables/arrange_fields/' . $form_state['values']['akst_machine_name']);  
    }
    else {
      drupal_goto('admin/structure/ak_scrolling_tables');    
    }
  }
  else {
    form_set_error('', t('Error while registering the new Scrolling Table.<br />PLEASE TALK TO THE WEBSITE ADMINISTRATOR.'));
  }  
}

/**
 * Adds the table definition to the database and
 * rebuilt the menu in order to construct the new path.
 * 
 * @param type $values
 * @return boolean
 * 
 */
function ak_scrolling_tables_add($values) {
  
  $fields = serialize($values['akst_chosen_ones']);
  
  if (trim($values['akst_path'])=='') {
    $values['akst_path'] = 'akst/' . $values['akst_machine_name'];
  }
  
  try {
    $result = db_insert('ak_scrolling_tables')
      ->fields(array(
        'name' => $values['akst_name'],
        'machine_name' => $values['akst_machine_name'],
        'description' =>  $values['akst_description'],          
        'content_type' => $values['akst_type'],          
        'fields' => $fields,
        'fields_order' => $fields,
        'path' => $values['akst_path'],
        'view_mode' => $values['akst_view_mode'],  
        'custom_view_mode' => $values['akst_custom_view_mode'],  
        'block' => $values['akst_block'],
      ))
      ->execute();
    //menu_rebuild();
    drupal_flush_all_caches();
    return TRUE;  
  } catch (Exception $e) {
    return FALSE;  
  }   
  
}

/**
 * Implements hook_form().
 * 
 * Form for adding tables.
 * 
 */
function ak_scrolling_tables_edit_form($form, &$form_state, $arg) {
   
  $table_info = ak_scrolling_tables_get_table_info($arg);
  
  $fields_list = ak_scrolling_tables_get_content_fields($table_info['content_type']);
  $chosen_fields = unserialize($table_info['fields']);
  $default_value = array();
  $i = 0;
  foreach ($fields_list['names'] as $field) {
    if (array_search($field, $chosen_fields)!==FALSE) {
      $default_value[] = $i;
    }
    $i++;
  }
  
  $entity_info = entity_get_info('node');
  $view_modes = $entity_info['view modes'];
  $view_modes_list = array(AK_SCROLLING_CUSTOM_TEXT => t('Custom'));
  foreach ($view_modes as $key => $view_mode) {
     $view_modes_list[$key]= $view_mode['label'];
  } 
  
  $form['akst_machine_name'] = array (
    '#type' => 'hidden',
    '#value' => $table_info['machine_name'],
  );    
  $form['akst_name'] = array (
    '#type' => 'hidden',
    '#value' => $table_info['name'],
  );    
  $form['akst_fields_order'] = array (
    '#type' => 'hidden',
    '#value' =>  unserialize($table_info['fields_order']),
  );      
  $form['akst_name2'] = array(
    '#type' => 'item',
    '#title' => t('Name'),      
    '#markup' => $table_info['name'] . '&nbsp;&nbsp;-&nbsp;&nbsp;<small>Machine name: ' . $table_info['machine_name'] . '</small>',      
  );     
  $form['content_type'] = array(
    '#type' => 'item',
    '#title' => t('Content type'),
    '#markup' => node_type_get_name($table_info['content_type']) . '&nbsp;&nbsp;-&nbsp;&nbsp;<small>Machine name: ' . $table_info['content_type'] . '</small>',      
  );   
  $form['akst_description'] = array(
    '#title' => t('Description'),
    '#value' => $table_info['description'],
    '#description' => t('A brief description of the Scrolling Table. (optional)'),  
    '#type' => 'textfield',
    '#size' => 120,
    '#maxlength' => 255,      
    '#required' => FALSE,
  ); 
  $form['akst_fields'] = array(
    '#title' => 'Fields',
    '#type' => 'checkboxes',
    '#options' => $fields_list['labels'],
    '#default_value' => $default_value,
    '#akst_machine_names' => $fields_list['names'],
    '#required' => TRUE,      
  );  
  $form['akst_path'] = array(
    '#title' => t('Custom Path'),
    '#value' => $table_info['path'],      
    '#description' => t('A unique path to the page displaying the Scrolling Table.<br />
      Optional. If not supplied the machine name will be used.'),  
    '#type' => 'textfield',
    '#size' => 64,
    '#maxlength' => 64,   
    '#required' => FALSE,
  );    
  $form['akst_view_mode'] = array(
    '#title' => t('View mode'),        
    '#type' => 'select',
    '#options' => $view_modes_list,
    '#value' => $table_info['view_mode'],      
    '#description' => t('The view mode to be used when displaying the Scrolling Table.<br />
      If "Custom" is chosen the name of a custom view mode should be supplied. '),   
    '#ajax' => array(
      'callback' => 'ak_scrolling_tables_add_custom_view_mode_ajax_callback',
      'wrapper' => 'akst-custom-view-mode',
    ),       
    '#required' => FALSE,
  );    
  $form['akst_custom_view_mode'] = array(
    '#title' => t('Custom view mode'),
    '#description' => t('A unique name for the custom view mode.'),  
    '#type' => 'hidden',
    '#value' => $table_info['custom_view_mode'],
    '#size' => 32,
    '#maxlength' => 32,   
    '#prefix' => '<div id="akst-custom-view-mode">',
    '#suffix' => '</div>',      
    '#required' => FALSE,
  );   
  $form['akst_block'] = array(
    '#title' => 'Create a Block',
    '#description' => t('Whether or not create a specific block for this Scrolling Table.'),      
    '#type' => 'checkbox',
    '#value' => $table_info['block'],
    '#default-value' => FALSE,
  ); 
  
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['add'] = array('#type' => 'submit', '#value' => t('Save'), '#name' => 'buttom_save');
  $form['actions']['arrange'] = array('#type' => 'submit', '#value' => t('Save and arrange fields'), '#name' => 'button_arrange');
  
  //Displays or hides the textfield for the name of the custom view mode
  if ((isset($form_state['input']['akst_view_mode']) && 
      $form_state['input']['akst_view_mode']==AK_SCROLLING_CUSTOM_TEXT) ||          
      (!isset($form_state['input']['akst_view_mode']) && 
      $table_info['view_mode']==AK_SCROLLING_CUSTOM_TEXT)) {
    $form['akst_custom_view_mode']['#type'] = 'textfield';
    $form['akst_custom_view_mode']['#required'] = TRUE;     
  } 
  else {
    $form['akst_custom_view_mode']['#type'] = 'hidden';  
  }
   
  return $form;  
  
}

/**
 * Implements hook_form_validade
 * 
 * @return boolean from ak_scrolling_tables_general__form_validation(), which
 * is commom to Add and Edit forms.
 * 
 */
function ak_scrolling_tables_edit_form_validate($form, &$form_state) {
  //Check if the path is already in use
  //return ak_scrolling_tables_general__form_validation($form_state); 
  //dvm($form_state);
  if ($form_state['input']['akst_view_mode']==AK_SCROLLING_CUSTOM_TEXT && trim($form_state['input']['akst_custom_view_mode'])=='') {
    form_set_error('akst_custom_view_mode', t('Custom view mode field is required.'));          
  }
  if ($form_state['input']['akst_path']!=$form_state['values']['akst_path'] &&
        trim($form_state['input']['akst_path'])!='' && 
        drupal_valid_path($form_state['input']['akst_path'], FALSE)) {
    form_set_error('akst_path', t('Invalid Custom Path.'));    
  }  
  $form_state['input'] = ak_scrolling_tables_sanitize_form($form, $form_state['input']);
}


/**
 * Query to get the table info  
 */
function ak_scrolling_tables_get_table_info($arg) {
  
  $query = db_select('ak_scrolling_tables', 'a')
    ->fields('a')
    ->condition('a.machine_name', $arg);  
  
  $result = $query->execute()->fetchAssoc();
  
  if ($result) {
    if ($result['fields_order']=='') {
      $result['fields_order'] = $result['fields'];
    }      
    return $result;
  }
  else {
    return FALSE;
  }
  
}

/**
 * Implements hook_form_submit
 * 
 * Variables:
 * 
 * $form_state['values']['akst_chosen_ones']: A non standart form element  
 * to be sent to ak_scrolling_tables_add().
 * 
 */
function ak_scrolling_tables_edit_form_submit($form, &$form_state) {
  $fields = $form_state['values']['akst_fields'];
  $chosen_ones = array();
  //Count how many fields were chosen
  foreach ($fields as $key => $checked) {
    if ($checked!='') {
      $chosen_ones[] = $form['akst_fields']['#akst_machine_names'][$key];
    }
  }  
  //Creates a non standart element in $form_state['input'] in order to be
  //sent to ak_scrolling_tables_add.
  $form_state['input']['akst_chosen_ones'] = $chosen_ones;  
  //Save editing
  if (ak_scrolling_tables_edit($form_state['input']))  {
    drupal_set_message(t('The scrolling table ' . $form_state['values']['akst_name'] . ' has been updated.' ));
    //Check if need to remove blocks
    if (!isset($form_state['input']['akst_block']) || $form_state['input']['akst_block']==FALSE) {
      ak_scrolling_tables_remove_blocks($form_state['input']['akst_machine_name']);
    }
    //Check in user chose fields arrangement
    if (isset($form_state['values']['button_arrange'])) {      
      drupal_goto('admin/structure/ak_scrolling_tables/arrange_fields/' . $form_state['values']['akst_machine_name']);  
    }
    else {
      drupal_goto('admin/structure/ak_scrolling_tables');    
    }
  }
  else {
    form_set_error('', t('Error while updating the Scrolling Table.<br />PLEASE TALK TO THE WEBSITE ADMINISTRATOR.'));
  }  
}

/**
 * Updates the table definition to the database and rebuilt the menu.
 * 
 * @param type $values
 * @return boolean
 * 
 */
function ak_scrolling_tables_edit($values) {
  
  $fields_order = ak_scrolling_tables_synchronize_fields($values['akst_chosen_ones'], $values['akst_fields_order']);
  
  if (trim($values['akst_path'])=='') {
    $values['akst_path'] = 'akst/' . $values['akst_machine_name'];
  }
  
  try {
    $result = db_update('ak_scrolling_tables')
      ->fields(
        array(
        'description' =>  $values['akst_description'],          
        'fields' => serialize($values['akst_chosen_ones']),
        'fields_order' => serialize($fields_order),
        'path' => $values['akst_path'],
        'view_mode' => $values['akst_view_mode'],  
        'custom_view_mode' => $values['akst_custom_view_mode'],              
        'block' => (isset($values['akst_block'])) ? $values['akst_block'] : 0,
        )
      )
      ->condition('machine_name', $values['akst_machine_name'])      
      ->execute();
    //menu_rebuild();    
    drupal_flush_all_caches();
    return TRUE;  
  } catch (Exception $e) {
    return FALSE;  
  }    
  
}

function ak_scrolling_tables_synchronize_fields($chosen_ones, $order) {
  $fields_order = explode(chr(32), $order);  
  $result1 = array_intersect($fields_order, $chosen_ones);
  $result2 = array_diff($chosen_ones, $fields_order);
  return array_merge($result1, $result2);
}


/**
 * Remove all blocks generated by the module.
 * 
 * @param type $machine_name :
 * if supplied all blocks related to the specific table will be removed;
 * if not supplied all block related to the module will be removed (module unistall);
 * 
 */
function ak_scrolling_tables_remove_blocks($machine_name) {
  try {
    $query = db_delete('block')
      ->condition('delta', AK_SCROLLING_BLOCK_PREFIX . $machine_name)
      ->execute();
  } catch (Exception $e) {
    return FALSE;  
  }   
  return TRUE;  
}

/**
 * Implements hook_form().
 * 
 * Form for deleting tables.
 * 
 */
function ak_scrolling_tables_delete_form($form, &$form_state, $arg) {
  
  $table_info = ak_scrolling_tables_get_table_info($arg);
  
  $entity_info = entity_get_info('node');
  $view_modes = $entity_info['view modes'];  
  if ($table_info['view_mode']==AK_SCROLLING_CUSTOM_TEXT) {
  $view_mode = $table_info['custom_view_mode'];
  }
  else {
    $view_mode = $view_modes[$table_info['view_mode']]['label'];
  }  
  
  $fields_list = ak_scrolling_tables_get_content_fields($table_info['content_type']);
  $chosen_fields = unserialize($table_info['fields']);
  $i = 0;
  $fields = '<ul>';
  foreach ($fields_list['names'] as $field) {
    if (array_search($field, $chosen_fields)!==FALSE) {
      $fields .= '<li>' . $fields_list['labels'][$i] . '</li>';
    }
    $i++;
  }
  $fields .= '</ul>';
  
  $form['akst_machine_name'] = array (
    '#type' => 'hidden',
    '#value' => $table_info['machine_name'],
  );    
  $form['akst_name'] = array (
    '#type' => 'hidden',
    '#value' => $table_info['name'],
  );    
  $form['akst_block'] = array (
    '#type' => 'hidden',
    '#value' => $table_info['block'],
  );    
  $form['akst_name2'] = array(
    '#title' => t('Name'),      
    '#type' => 'item',
    '#markup' => $table_info['name'] . '&nbsp;&nbsp;-&nbsp;&nbsp;<small>Machine name: ' . $table_info['machine_name'] . '</small>',      
  );  
  $form['content_type'] = array(
    '#title' => t('Content type'),  
    '#type' => 'item',    
    '#markup' => node_type_get_name($table_info['content_type']) . '&nbsp;&nbsp;-&nbsp;&nbsp;<small>Machine name: ' . $table_info['content_type'] . '</small>',      
  );   
  $form['akst_description'] = array(
    '#title' => t('Description'),
    '#type' => 'item',      
    '#markup' => $table_info['description'],
  ); 
  $form['akst_fields'] = array(
    '#title' => t('Fields'),
    '#type' => 'item',      
    '#markup' => $fields,
  );  
  $form['akst_path'] = array(
    '#title' => t('Custom Path'),
    '#type' => 'item',      
    '#markup' => $table_info['path'],          
  );   
  $form['akst_path'] = array(
    '#title' => t('View mode'),
    '#type' => 'item',      
    '#markup' => $view_mode,          
  );   
  $form['akst_block2'] = array(
    '#title' => t('Block'),
    '#type' => 'item',      
    '#markup' => ($table_info['block']) ? t('Yes') : t('No'),          
  );   
   
  $caption = t('This action cannot be undone.');
  $heading = t('Are you sure you want to delete this Scrolling Table?');
  $cancel_path = 'admin/structure/ak_scrolling_tables'; 
  $yes = t('Delete');
  $no = t('Cancel');

  return confirm_form($form, $heading, $cancel_path, $caption, $yes, $no);     
  
}

function ak_scrolling_tables_delete_form_submit($form, &$form_state) {
  if (ak_scrolling_tables_delete($form_state['values']['akst_machine_name']))  {
    //Check if need to remove blocks
    if (isset($form_state['values']['akst_block']) && $form_state['values']['akst_block']==TRUE) {
      ak_scrolling_tables_remove_blocks($form_state['values']['akst_machine_name']);
    }
    drupal_set_message(t('The scrolling table ' . $form_state['values']['akst_name'] . ' has been deleted.' ));
    drupal_goto('admin/structure/ak_scrolling_tables');
  }
  else {
    form_set_error('', t('Error while deleting the Scrolling Table.<br />PLEASE TALK TO THE WEBSITE ADMINISTRATOR.'));
  }    
}


/**
 * Excludes the table definition from the database and rebuilt the menu.
 * 
 * @param type $arg
 * @return boolean
 */
function ak_scrolling_tables_delete($arg) {
  try {
    $result = db_delete('ak_scrolling_tables')
      ->condition('machine_name', $arg)      
      ->execute();
    //menu_rebuild();  
    drupal_flush_all_caches();
  } catch (Exception $e) {
    return FALSE;  
  }   
  return $result;
}

/**
* Implements hook_entity_info_alter().
*/
function ak_scrolling_tables_entity_info_alter(&$entity_info) {
  
  $tables = ak_scrolling_tables_get_all_tables(); 
  $view_modes_names = array();

  foreach ($tables as $table) {
    if ($table->view_mode==AK_SCROLLING_CUSTOM_TEXT && !in_array($table->custom_view_mode, $view_modes_names)) {
      $view_modes_names[] = $table->custom_view_mode;
      $machine_readable = ak_scrolling_tables_get_machine_name($table->custom_view_mode);
      $entity_info['node']['view modes'][$machine_readable] = array(
        'label' => $table->custom_view_mode,
        'custom settings' => FALSE,
      );      
    }
  }   

}

function ak_scrolling_tables_get_machine_name($human_readable) {
  return preg_replace('@[^a-z0-9_]+@','_',  strtolower($human_readable));
}

/**
 * Implements hook_block_info().
 */
function ak_scrolling_tables_block_info() {  
  
  $blocks = array();
  
  $tables = ak_scrolling_tables_get_all_tables();
  
  foreach ($tables as $table) {
    if ($table->block) {
      $blocks[AK_SCROLLING_BLOCK_PREFIX . $table->machine_name] = array(
        'info' => $table->name,
        'cache' => DRUPAL_NO_CACHE,
      );         
    }
  }
  
  return $blocks;
      
}

/**
 * Implements hook_block_view().
 */
function ak_scrolling_tables_block_view($delta) {  
 
  if (preg_match('/' . AK_SCROLLING_BLOCK_PREFIX . '/',$delta)) {
    $machine_name = str_replace(AK_SCROLLING_BLOCK_PREFIX, '', $delta);  
    $table_info = ak_scrolling_tables_get_table_info($machine_name);
    $block = array();
    $block['subject'] = $table_info['name'];
    $block['content'] = ak_scrolling_tables_display($machine_name, TRUE) . '<div style="clear: both;">&nbsp;</div>';  
    return $block; 
  }
  else {
    return FALSE;
  }
  
}