<?php

class AKDataTable {
 
  public $content_type;
  // $headers was kept for future versions that will be able
  // to have alternative headers configuration.
  public $headers = array();
  public $fields = array();
  public $query = '';
  public $id = 'akst-table';
  public $class_prefix = 'col';
  public $view_mode = 'full';
  
  //Initial tags
  function ini_tags() {    
    $ths = '';   
    // The TRUE block of the IF was kept for future versions that will be able
    // to have alternative headers configuration.
    if (count($this->headers )>0) {
      foreach ($this->headers as $title)  {
        $ths .= '<th>' . $title . '</th>';
      }
    }
    else {
      foreach ($this->fields as $field)   {
        if ($field=='title') {
          $node_info = node_type_get_type($this->content_type);
          if ($node_info->has_title) {
            $ths .= '<th>' . $node_info->title_label . '</th>';          
          } 
          else {
            $ths .= '<th>Title</th>';                      
          }          
        } 
        else {
          $field_info = field_info_instance('node', $field, $this->content_type);
          $ths .= '<th>' . $field_info['label'] . '</th>';          
        }
      }      
    }
    
    $output = '<table cellpadding="0" cellspacing="0" border="0" class="display" id="' . $this->id . '">';
    $output .= '<thead><tr>' . $ths . '</tr></thead>';
    //$output .= '<tfoot><tr>' . $ths . '</tr></tfoot>';  
    $output .= '<tbody>';
    return $output;
  }
  
  function end_tags() {
    return '</tbody></table>';
  }
  
  function table_rows() {
    $output = '';
    $nodes = node_load_multiple(array(), array('type' => $this->content_type));  
    if (count($nodes)>0 ) {
      foreach ($nodes as $node) {
        $tds = '';   
        $tdobj = new AKTableDataCell;
        $tdobj->view_mode = $this->view_mode;
        foreach ($this->fields as $field)  {
          $tdobj->field = $field;
          $tdobj->node = $node;
          $tds .= $tdobj->result();        
        }            
        $output .= '<tr>' . $tds . '</tr>';
      }     
      return $output;
    }
    else {
      return FALSE;
    }
  }
  
  function table() {
    $table_rows = $this->table_rows();;
    if ($table_rows) {
      $result = $this->ini_tags();
      $result .= $table_rows;
      $result .= $this->end_tags();
    }
    else {
      return '<div>' . t('No data found.') . '</div>';
    }
    return $result;
  }  
  
}

class AKTableDataCell {
  public $field = '';
  public $node;
  public $view_mode = 'full';
  function result() {
    $td = new AKDTCell;
    $td::$field = $this->field;
    $td::$node = $this->node;
    $td::$view_mode = $this->view_mode;
    if ($this->field=='title') {
      $class = 'AKDTCell_Title';
    }
    else {
      $field_info = field_info_field($this->field);
      $td::$settings = $field_info['settings'];
      $class = 'AKDTCell_' . $field_info['type'];      
    }
    if (class_exists($class)) {
      return $class::result();
    }
    else {
      return AKDTCell::result();
    }
  }  
}

class AKDTCell {
  public static $field;
  public static $node;
  public static $settings;
  public static $view_mode = 'full';
  static function build_td($value) {
    return '<td class="akst-td-cell">' . $value . '</td>';
  }
  static function result() {
    return self::build_td('n/a');
  }    
}

class AKDTCell_Title extends AKDTCell {
  static function result() {    
    $title = self::$node->title;
    $url = 'node/' . self::$node->nid;
    return self::build_td(l($title, $url));    
  }
}

class AKDTCell_text extends AKDTCell {
  static function result() {   
    $node = self::$node;
    $field = self::$field;
    $node_view = node_view(node_load($node->nid), self::$view_mode);
    if (isset($node_view[$field])) {
      $max_items = count($node_view[$field]['#items']);
      $value = '';
      for ($i=0; $i<$max_items; $i++) {
        $value .= $node_view[$field][$i]['#markup'];       
        if ($i<$max_items) {
          $value .= '<br />';
        }        
      }
    }
    else {
      $value = '';
    }
    return self::build_td($value);
  }
}

class AKDTCell_text_with_summary extends AKDTCell_text {  
}

class AKDTCell_datetime extends AKDTCell_text {
}

class AKDTCell_number_integer extends AKDTCell_text {
}

class AKDTCell_number_decimal extends AKDTCell_text {
}

class AKDTCell_number_float extends AKDTCell_text {
}

class AKDTCell_list_boolean extends AKDTCell_text {
}

class AKDTCell_list_float extends AKDTCell_text {
}

class AKDTCell_list_integer extends AKDTCell_text {
}

class AKDTCell_list_text extends AKDTCell_text {
}

class AKDTCell_text_long extends AKDTCell_text {
}

class AKDTCell_link_field extends AKDTCell {
  static function result() {   
    $node = self::$node;
    $field = self::$field;
    $node_view = node_view(node_load($node->nid),self::$view_mode);
    if (isset($node_view[$field])) { 
      $max_items = count($node_view[$field]['#items']);
      $value = '';
      for ($i=0; $i<$max_items; $i++) {
        $title = $node_view[$field]['#items'][$i]['title'];
        $url = $node_view[$field]['#items'][$i]['display_url'];
        $value .= l($title,$url);           
        if ($i<$max_items) {
          $value .= '<br />';
        }          
      }       
    }
    else {
      $value = '';
    }
    return self::build_td($value);
  }  
}

class AKDTCell_taxonomy_term_reference extends AKDTCell {
  static function result() {   
    $node = self::$node;
    $field = self::$field;
    $field_info = $node->$field;
    if (isset($field_info['und'])) {
      $tid = $field_info['und'][0]['tid'];
      $term = taxonomy_term_load($tid);
      $value = $term->name;
    }
    else {
      $value = '';
    }
    return self::build_td($value);
  }  
}

class AKDTCell_image extends AKDTCell {
  static function result() {   
    $node = self::$node;
    $field = self::$field;
    $node_view = node_view(node_load($node->nid),self::$view_mode);    
    if (isset($node_view[$field])) {
      $styles = image_styles();
      $img_style = $node_view[$field][0]['#image_style'];
      if ($img_style!='') {
        $effects = array_values($styles[$img_style]['effects']);
        $img_width = $effects[0]['data']['width'];
        $img_height = $effects[0]['data']['height'];      $max_items = count($node_view[$field]['#items']);
        $total_height = 0;
        $img_tag = '';
        for ($i=0; $i<$max_items; $i++) {        
          $uri = $node_view[$field][$i]['#item']['uri'];
          //@TODO: study a better way to calculate the height increase
          $total_height += $img_height + 5;
          $img_tag .= '<img src="'. image_style_url($img_style, $uri).'" /><br />';       
        }      
        $value = '<div style="min-width: ' . $img_width . 'px; min-height: ' . $total_height . 'px;">' . $img_tag . '</div>';       
      }
      else {
        $value = '';
      }
    }
    else {
      $value = '';
    }
    return self::build_td($value);
  }   
}
