(function ($) {

      $(document).ready( function () {
				var AKSTTableVAR = $(AKSTTableID).dataTable( {
					"sScrollY": "300px",
					"sScrollX": "100%",
					"sScrollXInner": "150%",
					"bScrollCollapse": true,
					"bPaginate": false       
				} );
				new FixedColumns( AKSTTableVAR );
			} );

})(jQuery);
